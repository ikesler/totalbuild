﻿using System;
using System.Configuration;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text.RegularExpressions;

namespace TotalBuild
{
    class Program
    {
        private static string devEnvPath;

        static void Main(string[] args)
        {
            var solutions = Directory.GetFiles(args[0], "*.sln", SearchOption.AllDirectories).ToList();

            Console.WriteLine("Started clean of " + solutions.Count + " solutions");

            foreach (var solution in solutions)
            {
                ExecDevEnv("\"" + solution + "\" /Clean");
            }

            Console.WriteLine("Solutions have been cleaned.");
            Console.WriteLine("Started build of " + solutions.Count + " solutions");

            var totalSolutions = solutions.Count;
            var i = 0;
            var anyHasBuilt = true;
            while (i < totalSolutions && anyHasBuilt)
            {
                anyHasBuilt = false;
                for (var j = solutions.Count - 1; j >= 0; --j)
                {
                    var exitCode = ExecDevEnv("/build Debug \"" + solutions[j] + "\"");
                    if (exitCode == 0)
                    {
                        var solution = solutions[j];
                        solutions.Remove(solution);
                        anyHasBuilt = true;
                        Console.WriteLine("Has built \"" + solution +"\"; " + solutions.Count + " solutions left.");
                    }
                }
                ++i;
            }

            Console.WriteLine("Finished. Failures " + solutions.Count);
            foreach (var solution in solutions)
            {
                Console.WriteLine(solution);
            }
        }

        static int ExecDevEnv(string commanLine)
        {
            DetectDevEnv();

            Process process = new Process
            {
                StartInfo = new ProcessStartInfo
                {
                    FileName = devEnvPath,
                    Arguments = commanLine,
                    WindowStyle = ProcessWindowStyle.Hidden
                }
            };
            process.Start();
            process.WaitForExit();
            return process.ExitCode;
        }

        static void DetectDevEnv()
        {
            if (devEnvPath != null)
                return;
            devEnvPath = ConfigurationManager.AppSettings["VisualStudioPath"];
            if (string.IsNullOrEmpty(devEnvPath))
            {
                var envVars = Environment.GetEnvironmentVariables();
                var vsTools = envVars.Keys.OfType<string>().Where(k =>
                {
                    var kLower = k.ToLower();
                    return kLower.StartsWith("vs") && kLower.EndsWith("comntools");
                }).ToList();
                if (vsTools.Count == 1)
                {
                    devEnvPath = envVars[vsTools[0]].ToString();
                    devEnvPath = Regex.Replace(devEnvPath,
                        @"Tools[\\/]?$", @"IDE\devenv.com",
                        RegexOptions.CultureInvariant | RegexOptions.IgnoreCase);
                }
            }
            if (!File.Exists(devEnvPath))
            {
                Console.WriteLine("Visual studio not found in location " + devEnvPath);
                throw new Exception();
            }
        }
    }
}
